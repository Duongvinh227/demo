import matplotlib.pyplot as plt
from PIL import Image
from tool.config import Cfg
from tool.translate import build_model, process_input, translate
import torch
import onnxruntime
import numpy as np

config = Cfg.load_config_from_file('./config/vgg-seq2seq.yml')
config['cnn']['pretrained']=False
config['device'] = 'cpu'
weight_path = 'vgg_seq2seq.pth'

# build model
model, vocab = build_model(config)

# load weight
model.load_state_dict(torch.load(weight_path, map_location=torch.device(config['device'])))
model = model.eval()

def convert_cnn_part(img, save_path, model):
    with torch.no_grad():
        src = model.cnn(img)
        torch.onnx.export(model.cnn, img, save_path, export_params=True, opset_version=12, do_constant_folding=True,
                          verbose=True, input_names=['img'], output_names=['output'],
                          dynamic_axes={'img': {3: 'lenght'}, 'output': {0: 'channel'}})

    return src

img = torch.rand(1, 3, 32, 475)
src = convert_cnn_part(img, './weights/cnn.onnx', model)

def convert_encoder_part(model, src, save_path):
    encoder_outputs, hidden = model.transformer.encoder(src)
    torch.onnx.export(model.transformer.encoder, src, save_path, export_params=True, opset_version=11,
                      do_constant_folding=True, input_names=['src'], output_names=['encoder_outputs', 'hidden'],
                      dynamic_axes={'src': {0: "channel_input"}, 'encoder_outputs': {0: 'channel_output'}})
    return hidden, encoder_outputs

hidden, encoder_outputs = convert_encoder_part(model, src, './weights/encoder.onnx')

def convert_decoder_part(model, tgt, hidden, encoder_outputs, save_path):
    tgt = tgt[-1]

    torch.onnx.export(model.transformer.decoder,
                      (tgt, hidden, encoder_outputs),
                      save_path,
                      export_params=True,
                      opset_version=11,
                      do_constant_folding=True,
                      input_names=['tgt', 'hidden', 'encoder_outputs'],
                      output_names=['output', 'hidden_out', 'last'],
                      dynamic_axes={'encoder_outputs': {0: "channel_input"},
                                    'last': {0: 'channel_output'}})


device = img.device
tgt = torch.LongTensor([[1] * len(img)]).to(device)
convert_decoder_part(model, tgt, hidden, encoder_outputs, './weights/decoder.onnx')



