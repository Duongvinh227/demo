from PIL import Image
from vietocr.tool.predictor import Predictor
from vietocr.tool.config import Cfg
0000
# config = Cfg.load_config_from_file('config.yml')
config = Cfg.load_config_from_name('vgg_transformer')
config['weights'] = 'transformerocr.pth'
config['device'] = 'cpu'
config['cnn']['pretrained']=False
config['predictor']['beamsearch']=False
detector = Predictor(config)

img = 'A.PNG'
img = Image.open(img)
# dự đoán
s = detector.predict(img)
print(s)