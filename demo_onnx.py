import time
import matplotlib.pyplot as plt
from PIL import Image
from tool.config import Cfg
from tool.translate import build_model, process_input, translate
import torch
import onnxruntime
import numpy as np

config = Cfg.load_config_from_file('./config/vgg-seq2seq.yml')
config['cnn']['pretrained']=False
config['device'] = 'cpu'
model, vocab = build_model(config)

def translate_onnx(img, session, max_seq_length=128, sos_token=1, eos_token=2):
    cnn_session, encoder_session, decoder_session = session

    # create cnn input
    cnn_input = {cnn_session.get_inputs()[0].name: img}
    src = cnn_session.run(None, cnn_input)

    # create encoder input
    encoder_input = {encoder_session.get_inputs()[0].name: src[0]}
    encoder_outputs, hidden = encoder_session.run(None, encoder_input)
    translated_sentence = [[sos_token] * len(img)]
    max_length = 0

    while max_length <= max_seq_length and not all(
            np.any(np.asarray(translated_sentence).T == eos_token, axis=1)
    ):
        tgt_inp = translated_sentence
        decoder_input = {decoder_session.get_inputs()[0].name: tgt_inp[-1],
                         decoder_session.get_inputs()[1].name: hidden,
                         decoder_session.get_inputs()[2].name: encoder_outputs}

        output, hidden, _ = decoder_session.run(None, decoder_input)
        output = np.expand_dims(output, axis=1)
        output = torch.Tensor(output)

        values, indices = torch.topk(output, 1)
        indices = indices[:, -1, 0]
        indices = indices.tolist()

        translated_sentence.append(indices)
        max_length += 1

        del output

    translated_sentence = np.asarray(translated_sentence).T

    return translated_sentence

# create inference session
cnn_session = onnxruntime.InferenceSession("./weights/cnn.onnx")
encoder_session = onnxruntime.InferenceSession("./weights/encoder.onnx")
decoder_session = onnxruntime.InferenceSession("./weights/decoder.onnx")
def handle(path_img):
    img = Image.open(path_img)
    img = process_input(img, config['dataset']['image_height'], config['dataset']['image_min_width'], config['dataset']['image_max_width'])
    img = img.to(config['device'])
    return img

def func_predict(img_precdict, session):
    start_time = time.time()
    s = translate_onnx(np.array(img_precdict), session)[0].tolist()
    s = vocab.decode(s)
    print("Result: ", s)
    end_time = time.time()
    print(end_time - start_time)

session = (cnn_session, encoder_session, decoder_session)

# image_demo = handle("process_image/label_1.png")

# image_nsx = handle("nsx.png")
# image_C = handle("C.png")
image_demo = handle("A.PNG")

# func_predict(image_nsx)
func_predict(image_demo,session)
# func_predict(image_A)
# func_predict(image_C)


