import threading
import time

import cv2
import matplotlib.pyplot as plt
from PIL import Image
from tool.config import Cfg
from tool.translate import build_model, process_input, translate
import torch
import onnxruntime
import numpy as np

config = Cfg.load_config_from_file('./config/vgg-seq2seq.yml')
config['cnn']['pretrained']=False
config['device'] = 'cpu'
model, vocab = build_model(config)

def translate_onnx(img, session, max_seq_length=128, sos_token=1, eos_token=2):
    cnn_session, encoder_session, decoder_session = session

    # create cnn input
    cnn_input = {cnn_session.get_inputs()[0].name: img}
    src = cnn_session.run(None, cnn_input)

    # create encoder input
    encoder_input = {encoder_session.get_inputs()[0].name: src[0]}
    encoder_outputs, hidden = encoder_session.run(None, encoder_input)
    translated_sentence = [[sos_token] * len(img)]
    max_length = 0

    while max_length <= max_seq_length and not all(
            np.any(np.asarray(translated_sentence).T == eos_token, axis=1)
    ):
        tgt_inp = translated_sentence
        decoder_input = {decoder_session.get_inputs()[0].name: tgt_inp[-1],
                         decoder_session.get_inputs()[1].name: hidden,
                         decoder_session.get_inputs()[2].name: encoder_outputs}

        output, hidden, _ = decoder_session.run(None, decoder_input)
        output = np.expand_dims(output, axis=1)
        output = torch.Tensor(output)

        values, indices = torch.topk(output, 1)
        indices = indices[:, -1, 0]
        indices = indices.tolist()

        translated_sentence.append(indices)
        max_length += 1

        del output

    translated_sentence = np.asarray(translated_sentence).T

    return translated_sentence

# create inference session
cnn_session = onnxruntime.InferenceSession("./weights/cnn.onnx")
encoder_session = onnxruntime.InferenceSession("./weights/encoder.onnx")
decoder_session = onnxruntime.InferenceSession("./weights/decoder.onnx")

def handle(img):
    img = process_input(img, config['dataset']['image_height'], config['dataset']['image_min_width'], config['dataset']['image_max_width'])
    img = img.to(config['device'])
    return img

def func_predict(img_precdict, session):
    # start_time = time.time()
    s = translate_onnx(np.array(img_precdict), session)[0].tolist()
    s = vocab.decode(s)
    # print("Result: ", s)
    # end_time = time.time()
    # print(end_time - start_time)

cnn_session_2 = onnxruntime.InferenceSession("./weights/label_1/cnn_1.onnx")
encoder_session_2 = onnxruntime.InferenceSession("./weights/label_1/encoder_1.onnx")
decoder_session_2 = onnxruntime.InferenceSession("./weights/label_1/decoder_1.onnx")
cnn_session_3 = onnxruntime.InferenceSession("./weights/label_1/cnn_2.onnx")
encoder_session_3 = onnxruntime.InferenceSession("./weights/label_1/encoder_2.onnx")
decoder_session_3 = onnxruntime.InferenceSession("./weights/label_1/decoder_2.onnx")
cnn_session_4 = onnxruntime.InferenceSession("./weights/label_1/cnn_3.onnx")
encoder_session_4 = onnxruntime.InferenceSession("./weights/label_1/encoder_3.onnx")
decoder_session_4 = onnxruntime.InferenceSession("./weights/label_1/decoder_3.onnx")
cnn_session_5 = onnxruntime.InferenceSession("./weights/label_1/cnn_4.onnx")
encoder_session_5 = onnxruntime.InferenceSession("./weights/label_1/encoder_4.onnx")
decoder_session_5 = onnxruntime.InferenceSession("./weights/label_1/decoder_4.onnx")
cnn_session_6 = onnxruntime.InferenceSession("./weights/label_1/cnn_5.onnx")
encoder_session_6 = onnxruntime.InferenceSession("./weights/label_1/encoder_5.onnx")
decoder_session_6 = onnxruntime.InferenceSession("./weights/label_1/decoder_5.onnx")
cnn_session_7 = onnxruntime.InferenceSession("./weights/label_1/cnn_6.onnx")
encoder_session_7 = onnxruntime.InferenceSession("./weights/label_1/encoder_6.onnx")
decoder_session_7 = onnxruntime.InferenceSession("./weights/label_1/decoder_6.onnx")

session_1 = (cnn_session, encoder_session, decoder_session)
session_2 = (cnn_session_2, encoder_session_2, decoder_session_2)
session_3 = (cnn_session_3, encoder_session_3, decoder_session_3)
session_4 = (cnn_session_4, encoder_session_4, decoder_session_4)
session_5 = (cnn_session_5, encoder_session_5, decoder_session_5)
session_6 = (cnn_session_6, encoder_session_6, decoder_session_6)
session_7 = (cnn_session_7, encoder_session_7, decoder_session_7)

start_time = time.time()

img_label1 = cv2.imread("process_image/label_1.png")

img_detec_1 = handle(img_label1[43:54, 48:216])
img_detec_2 = handle(img_label1[31:41, 62:195])
img_detec_3 = handle(img_label1[22:32, 48:208])
img_detec_4 = handle(img_label1[11:23, 64:193])
img_detec_5 = handle(img_label1[23:35, 227:296])
img_detec_6 = handle(img_label1[35:46, 226:300])
img_detec_7 = handle(img_label1[5:17, 4:37])

t1 = threading.Thread(target=func_predict,args=(img_detec_1, session_1,))
t2 = threading.Thread(target=func_predict,args=(img_detec_2, session_2,))
t3 = threading.Thread(target=func_predict,args=(img_detec_3, session_3,))
t4 = threading.Thread(target=func_predict,args=(img_detec_4, session_4,))
t5 = threading.Thread(target=func_predict,args=(img_detec_5, session_5,))
t6 = threading.Thread(target=func_predict,args=(img_detec_6, session_6,))
t7 = threading.Thread(target=func_predict,args=(img_detec_7, session_7,))

t1.start()
t2.start()
t3.start()
t4.start()
t5.start()
t6.start()
t7.start()

t1.join()
t2.join()
t3.join()
t4.join()
t5.join()
t6.join()
t7.join()
end_time = time.time()
print(end_time - start_time)